package com.pietro.materialdesign;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.ToggleDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.OnCheckedChangeListener;
import com.pietro.materialdesigncard.R;

/**
 * Created by pietrogirardi on 8/23/15.
 */
public class MyNavegationDrawer implements OnCheckedChangeListener{

    private Activity activity;
    Bundle savedInstanceState;

    private Drawer.Result navegationDrawerRight;
    private Drawer.Result navegationDrawerLeft;
    private AccountHeader.Result headerNavegationLeft;
    private int mPositionClicked;

    public MyNavegationDrawer(Activity activity) {
        this.activity = activity;
    }

    public void addNavegationDrawerLeft(Toolbar toolbar, Bundle savedInstanceState){

        navegationDrawerLeft = new Drawer()
                .withActivity(activity)
                .withToolbar(toolbar)
                .withDisplayBelowToolbar(true)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstanceState)
                .withAccountHeader(addAccountHeaderLeft())
                .withSelectedItem(0)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        Toast.makeText(activity, "intem position = " + i, Toast.LENGTH_LONG).show();

                        for (int count = 0, tam = navegationDrawerLeft.getDrawerItems().size(); count < tam; count++) {
                            if (count == mPositionClicked && mPositionClicked <= 3) {
                                PrimaryDrawerItem aux = (PrimaryDrawerItem) navegationDrawerLeft.getDrawerItems().get(count);
                                aux.setIcon(activity.getResources().getDrawable(getCorretcDrawerIcon(count, false)));
                                break;
                            }
                        }

                        if(i <= 3){
                            ((PrimaryDrawerItem) iDrawerItem).setIcon(activity.getResources().getDrawable( getCorretcDrawerIcon( i, true ) ));
                        }

                        mPositionClicked = i;
                        navegationDrawerLeft.getAdapter().notifyDataSetChanged();
                    }
                })
                .build();

        addNavegationDrawerLeftItems(navegationDrawerLeft);
    }

    private AccountHeader.Result addAccountHeaderLeft(){
        headerNavegationLeft = new AccountHeader()
                .withActivity(activity)
                .withCompactStyle(false)
                .withSavedInstance(savedInstanceState)
                .withThreeSmallProfileImages(true)
                .withHeaderBackground(R.drawable.ct6)
                .addProfiles(
                        new ProfileDrawerItem().withName("Person One").withEmail("person1@gmail.com").withIcon(activity.getResources().getDrawable(R.drawable.person_1)),
                        new ProfileDrawerItem().withName("Person Two").withEmail("person2@gmail.com").withIcon(activity.getResources().getDrawable(R.drawable.person_2)),
                        new ProfileDrawerItem().withName("Person Three").withEmail("person3@gmail.com").withIcon(activity.getResources().getDrawable(R.drawable.person_3)),
                        new ProfileDrawerItem().withName("Person Four").withEmail("person4@gmail.com").withIcon(activity.getResources().getDrawable(R.drawable.person_4))
                        )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile iProfile, boolean b) {
                        Toast.makeText(activity, "onProfileChanged: " + iProfile.getName(), Toast.LENGTH_SHORT).show();
                        headerNavegationLeft.setBackgroundRes(R.drawable.vyron);
                        return false;
                    }
                })
                .build();
        return headerNavegationLeft;
    }

    public void addNavegationDrawerRight(Toolbar toolbar, Bundle savedInstanceState){

        navegationDrawerRight = new Drawer()
                .withActivity(activity)
                //.withToolbar(toolbar)
                .withDisplayBelowToolbar(false)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.RIGHT)
                .withSavedInstance(savedInstanceState)
                .withSelectedItem(0)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        Toast.makeText(activity, "intem position = " + i, Toast.LENGTH_LONG).show();

                        for (int count = 0, tam = navegationDrawerRight.getDrawerItems().size(); count < tam; count++) {
                            if (count == mPositionClicked && mPositionClicked <= 3) {
                                PrimaryDrawerItem aux = (PrimaryDrawerItem) navegationDrawerRight.getDrawerItems().get(count);
                                aux.setIcon(activity.getResources().getDrawable(getCorretcDrawerIcon(count, false)));
                                break;
                            }
                        }

                        if(i <= 3){
                            ((PrimaryDrawerItem) iDrawerItem).setIcon(activity.getResources().getDrawable( getCorretcDrawerIcon( i, true ) ));
                        }
                        mPositionClicked = i;
                        navegationDrawerRight.getAdapter().notifyDataSetChanged();
                    }
                })
                .build();

        addNavegationDrawerRightItems(navegationDrawerRight);
    }


    private void addNavegationDrawerLeftItems(Drawer.Result drawer){
        drawer.addItem(new PrimaryDrawerItem().withName("Carros Esportivos").withIcon(R.drawable.car_selected_1));
        drawer.addItem(new PrimaryDrawerItem().withName("Carros de luxo").withIcon(R.drawable.car_2));
        drawer.addItem(new DividerDrawerItem());
        drawer.addItem(new PrimaryDrawerItem().withName("Carros para colecionadores").withIcon(R.drawable.car_3));
        drawer.addItem(new PrimaryDrawerItem().withName("Carros populares").withIcon(R.drawable.car_4));
        drawer.addItem(new SectionDrawerItem().withName("Configurações"));
        drawer.addItem(new SwitchDrawerItem().withName("Notificações").withChecked(true).withOnCheckedChangeListener(this));
        drawer.addItem(new ToggleDrawerItem().withName("News").withChecked(true).withOnCheckedChangeListener(this));
    }

    private void addNavegationDrawerRightItems(Drawer.Result drawer){
        drawer.addItem(new SecondaryDrawerItem().withName("Carros Esportivos").withIcon(R.drawable.car_selected_1));
        drawer.addItem(new SecondaryDrawerItem().withName("Carros de Luxo").withIcon(R.drawable.car_2));
        drawer.addItem(new SecondaryDrawerItem().withName("Carros para Colecionadores").withIcon(R.drawable.car_3));
        drawer.addItem(new SecondaryDrawerItem().withName("Carros Populares").withIcon(R.drawable.car_4));
    }


    private int getCorretcDrawerIcon(int position, boolean isSelecetd){
        switch(position){
            case 0:
                return( isSelecetd ? R.drawable.car_selected_1 : R.drawable.car_1 );
            case 1:
                return( isSelecetd ? R.drawable.car_selected_2 : R.drawable.car_2 );
            case 2:
                return( isSelecetd ? R.drawable.car_selected_3 : R.drawable.car_3 );
            case 3:
                return( isSelecetd ? R.drawable.car_selected_4 : R.drawable.car_4 );
        }
        return(0);
    }

    @Override
    public void onCheckedChanged(IDrawerItem iDrawerItem, CompoundButton compoundButton, boolean b) {
        Toast.makeText(activity, "onCheckedChanged = "+ (b ? "true" : "false"), Toast.LENGTH_SHORT).show();
    }

    public Drawer.Result getNavegationDrawerLeft(){
        return navegationDrawerLeft;
    }
}
